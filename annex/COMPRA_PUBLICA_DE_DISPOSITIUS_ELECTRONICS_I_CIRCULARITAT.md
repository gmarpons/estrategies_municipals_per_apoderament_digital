## Propostes referents a compra pública de dispositius electrònics i circularitat

En el moment de determinar les condicions de compra pública d'un dispositiu electrònic és quan es té la possibilitat de influir en les condicions de fabricació del producte però també en determinar les condicions per la reutilizació d'aquest un cop es deixi de fer servir, per esgotar del tot la seva vida útil, i garantir el reciclatge final (economia circular).

**Objectius**
 * Tendir cap a compra de dispositius electrònics que minimitzi l’impacte negatiu en les persones i el medi ambient.

**Proposta programàtica**
 * Promoció de la reducció de l’impacte medi-ambiental i l’economia social i solidaria.
 * Respecte als drets laborals dels treballadors involucrats en la fabricació, manteniment, recollida i reciclatge de dispositius electrònics.
 * Compra pública responsable que respecti els drets laborals en la producció de béns electrònics.
 * Traçabilitat en origen dels dispositius per assegurar la circularitat: reparació, renovació, reutilització, garantir el reciclatge final i la transparència de les dades sobre aquests processos.
 * Compromís de donació dels dispositius a entitats socials al final de la seva primera vida útil, per allargar la seva vida útil i crear beneficis socials amb la reutilització.
 * Compra pública amb extensió de garantia que inclogui reparació i manteniment inclòs que permeti estendre la vida útil dels dispositius.
 * Promoció del coneixement sobre la reparació dels dispositius als centres educatius (escoles, universitats).

**Experiències inspiradores**

 * Condicions contractuals d'Electronics Watch: http://electronicswatch.org/ca/les-condicions-contractuals-d-electronics-watch_2459984 http://electronicswatch.org/ew-code-cat_2519765.pdf (Setem.cat, ElectronicsWatch.org)
 * Circuit pangea i ereuse.org, http://ereuse.org
 * Entitats socials que treballen en reutilizació: https://www.ereuse.org/community/
 * Donació d’ordinadors i mòbils a entitats socials per usos socials: cas Barcelona Activa, Ajuntament de Barcelona, Abacus Cooperativa, http://ajuntament.barcelona.cat/premsa/2018/06/23/lajuntament-de-barcelona-cedeix-a-entitats-socials-dispositius-tecnologics-en-desus/
 * Auto-reparació: Restarters Barcelona, http://restartersbcn.info/ https://therestartproject.org/restart-at-school/

**Males pràctiques**
 * Compra exclusivament per criteris tècnics
 * Mecanismes de renting on els equips es retiren sense garanties
 * Venda o donació de dispositius no utilitzats sense garanties d’esborrat de dades, d’us successiu i reciclatge final